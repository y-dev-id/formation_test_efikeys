package fr.devid.app.api

data class LoginDto(val email: String, val password: String)

data class LoginResponseDto(val token: String?)

data class RegisterDto(
    val name: String,
    val firstname: String,
    val email: String,
    val password: String
)

data class TokenDto(val token: String?)

data class AddressDto(
    val addressEntered: String,
    val streetNumber: String?,
    val route: String?,
    val postalCode: String,
    val city: String,
    val department: String,
    val region: String,
    val country: String,
    val latitude: String,
    val longitude: String
)

data class ProfileDto(
    val identificationCode: String?,
    val company: String?,
    val isPro: Boolean,
    val email: String,
    val name: String,
    val firstname: String,
    val phone: String,
    val address: List<AddressDto>
)

data class Garage(
    val id: Long,
    val name: String,
    val status: GarageStatusEnum,
    val imageUrl: String,
) {
    companion object {
        val SAMPLE: Garage = Garage(
            100,
            "Garage des potiers",
            GarageStatusEnum.INDEPENDENT,
            "https://media.playmobil.com/i/playmobil/4318_product_detail/Garage%20de%20la%20maison?locale=fr-FR,fr,*&\$pdp_product_zoom_xl\$&strip=true&qlt=80&fmt.jpeg.chroma=1,1,1&unsharp=0,1,1,7&fmt.jpeg.interlaced=true"
        )

        val SAMPLES: List<Garage> = listOf(
            SAMPLE.copy(id = 200),
            SAMPLE.copy(name = "Speedy Charles de Fitte"),
            SAMPLE.copy(id = 300, status = GarageStatusEnum.CONSTRUCTOR)
        )
    }
}

enum class GarageStatusEnum {
    INDEPENDENT,
    CONSTRUCTOR,
}

data class Vehicle(
    val licensePlate: String,
    val type: String,
    val imageUrls: List<String> = emptyList(),
    val kilometer: Int,
    val chassis: String,
    val contactName: String,
    val contactPhone: String,
    val clientRemarks: String,
    val keyStatus: KeyStatusEnum,
    val collectedBy: String?,
    val boxStatus: BoxStatusEnum,
) {
    companion object {
        val SAMPLE: Vehicle = Vehicle(
            "AB-123-CD",
            "Mini Cooper Essence",
            listOf(
                "https://www.automobile-magazine.fr/asset/cms/178540/config/127282/cooper.jpg",
                "https://www.automobile-magazine.fr/asset/cms/178540/config/127282/cooper.jpg"
            ),
            4240,
            "AF456888888YHT65777",
            "Pierre Dupont",
            "06 22 12 09 14",
            "Levier de vitesse dur",
            KeyStatusEnum.AVAILABLE,
            "Stéphane G.",
            BoxStatusEnum.USED,
        )

        val SAMPLES: List<Vehicle> = listOf(
            SAMPLE.copy(
                licensePlate = "HJ-758-OS",
                keyStatus = KeyStatusEnum.COLLECTED
            ),
            SAMPLE.copy(
                licensePlate = "AC-456-ZX",
                keyStatus = KeyStatusEnum.COLLECTED,
                collectedBy = "Eric G.",
                boxStatus = BoxStatusEnum.USED
            ),
            SAMPLE.copy(
                licensePlate = "US-647-ZG",
                keyStatus = KeyStatusEnum.COLLECTED
            ),
        )
    }
}

enum class KeyStatusEnum {
    AVAILABLE,
    COLLECTED,
}

enum class BoxStatusEnum {
    INBOX,
    INBOX_DOCK,
    USED
}
