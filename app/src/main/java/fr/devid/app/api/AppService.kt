package fr.devid.app.api

import fr.devid.app.service.AuthenticationTokenInterceptor
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface AppService {

    @POST("login")
    suspend fun login(@Body loginDto: LoginDto): Response<LoginResponseDto>?

    @POST("register")
    suspend fun register(@Body registerDto: RegisterDto): Response<TokenDto>?

    @GET("profile")
    @Headers(AuthenticationTokenInterceptor.AUTHORIZATION_HEADER)
    suspend fun getProfile(): ProfileDto?
}
