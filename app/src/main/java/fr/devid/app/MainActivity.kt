package fr.devid.app

import android.os.Bundle
import dagger.hilt.android.AndroidEntryPoint
import fr.devid.app.base.BaseActivity

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
