package fr.devid.app.ui.selection

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import fr.devid.app.R
import fr.devid.app.api.Garage
import fr.devid.app.databinding.FragmentMainBinding
import fr.devid.app.databinding.FragmentSelectionGarageBinding
import fr.devid.app.ui.main.MainViewModel
import fr.devid.app.ui.selection.recycler.GarageAdapter
import timber.log.Timber


class SelectionGarageFragment : Fragment() {
    private var _binding : FragmentSelectionGarageBinding?= null
    val binding
        get() = _binding!!
    lateinit var garageAdapter : GarageAdapter
    private val myViewModel : MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSelectionGarageBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = findNavController()

        garageAdapter = GarageAdapter()

        binding.recyclerViewId.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = garageAdapter
            scrollToPosition(garageAdapter.itemCount -1)
        }

        myViewModel.garagesLiveData.observe(viewLifecycleOwner){
            garageAdapter.submitList(it)
        }

        binding.btnActivityConfirm.setOnClickListener {
            myViewModel.updateCurrentGarage(garageAdapter.currentList[garageAdapter.selectedPosition])
            navController.popBackStack()
        }

        myViewModel.currentGarageLiveData.observe(viewLifecycleOwner){
            val listGarage = myViewModel.garagesLiveData.value
            val indexOfGarage = listGarage?.indexOf(it)
            garageAdapter.selectedPosition = indexOfGarage!!
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}
