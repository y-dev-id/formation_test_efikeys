package fr.devid.app.ui.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.devid.app.R
import fr.devid.app.api.AppService
import fr.devid.app.api.RegisterDto
import fr.devid.app.service.AuthenticationTokenInterceptor
import fr.devid.app.viewmodels.Event
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val authenticationTokenInterceptor: AuthenticationTokenInterceptor,
    private val appService: AppService
) : ViewModel() {

    enum class RegisterViewModelState {
        IDLE, LOADING, SUCCESS
    }

    val state: LiveData<RegisterViewModelState>
        get() = _state
    val errorMessageEvent = MutableLiveData<Event<String>>()
    val errorMessageResourceEvent = MutableLiveData<Event<Int>>()

    private val _state = MutableLiveData(RegisterViewModelState.IDLE)

    fun register(registerDto: RegisterDto, confirmationPassword: String) {
        if (registerDto.name.isBlank() || registerDto.firstname.isBlank() || registerDto.email.isBlank() || registerDto.password.isBlank()
        ) {
            errorMessageResourceEvent.value = Event(R.string.fill_all_fields)
            return
        }
        if (registerDto.password != confirmationPassword) {
            errorMessageResourceEvent.value = Event(R.string.different_password)
            return
        }

        _state.value = RegisterViewModelState.LOADING
        viewModelScope.launch {
            val response = appService.register(registerDto)
            _state.value = RegisterViewModelState.IDLE
            val body = response?.body()
            Timber.d("Register response = ${response?.code()}")
            if (body == null) {
                errorMessageResourceEvent.value = Event(R.string.check_internet)
                return@launch
            }
            if (body.token != null) {
                authenticationTokenInterceptor.token = body.token
                _state.value = RegisterViewModelState.SUCCESS
                return@launch
            }

            errorMessageEvent.value = Event("Une erreur est survenue")
        }
    }
}
