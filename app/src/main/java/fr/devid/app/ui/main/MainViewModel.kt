package fr.devid.app.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.devid.app.api.Garage
import fr.devid.app.api.Vehicle


class MainViewModel : ViewModel() {

    private val _garagesLiveData = MutableLiveData<List<Garage>>()
    val garagesLiveData: LiveData<List<Garage>> = _garagesLiveData

    private val _vehiclesLiveData = MutableLiveData<List<Vehicle>>()
    val vehiclesLiveData: LiveData<List<Vehicle>> = _vehiclesLiveData

    private val _currentGarageLiveData = MutableLiveData<Garage>()
    val currentGarageLiveData: LiveData<Garage> = _currentGarageLiveData

    init {
        _garagesLiveData.value = Garage.SAMPLES
        _vehiclesLiveData.value = Vehicle.SAMPLES
        _currentGarageLiveData.value = Garage.SAMPLES.first()
    }

    fun updateCurrentGarage(garage: Garage) {
        _currentGarageLiveData.value = garage
    }

}
