package fr.devid.app.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import fr.devid.app.R
import fr.devid.app.api.KeyStatusEnum
import fr.devid.app.base.BaseFragment
import fr.devid.app.databinding.FragmentHomeBinding
import fr.devid.app.ui.login.LoginViewModel
import fr.devid.app.ui.main.MainViewModel
import timber.log.Timber

@AndroidEntryPoint
class HomeFragment : BaseFragment() {

    private val loginViewModel: LoginViewModel by activityViewModels()
    private val mainViewModel: MainViewModel by activityViewModels()
    private val profileViewModel: ProfileViewModel by hiltNavGraphViewModels(R.id.tab_nav_graph)
    private val myListAdapter: DashboardVehiculeListAdapter = DashboardVehiculeListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.rvDashboard.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = myListAdapter
        }

        bindUi(binding)
        subscribeUi(binding)

        return binding.root
    }

    private fun bindUi(binding: FragmentHomeBinding) {
        binding.ibLogout.setOnClickListener {
            loginViewModel.logout()
        }
    }

    private fun subscribeUi(binding: FragmentHomeBinding) {
        val navController = findNavController()
        val navToSelection = HomeFragmentDirections.actionHomeFragmentToSelectionGarageFragment()

        profileViewModel.profile.observe(viewLifecycleOwner, {
            Timber.d("Profile received: $it")
            // binding.btCredits.isVisible = true
        })

        binding.btnDashboardChangeSite.text = getString(R.string.dashboard_change_site)
        binding.btnDashboardAddVehicule.text = getString(R.string.dashboard_add_vehicule)

        myListAdapter.onClickVehicle = {
            navController.navigate(R.id.action_homeFragment_to_detailsKeyFragment)
        }
        //Current Garage
        mainViewModel.currentGarageLiveData.observe(viewLifecycleOwner, { currentGarage ->
            Glide.with(this).load(currentGarage.imageUrl).into(binding.ivDashboardGarage)
            binding.tvDashboardGarageName.text = currentGarage.name
        })

        mainViewModel.vehiclesLiveData.observe(viewLifecycleOwner, { listOfVehicules ->

            val listOfCurrentVehiculesWithKeys =
                listOfVehicules.filter { it.keyStatus == KeyStatusEnum.COLLECTED }
            val nbrOfKeyCollected = listOfCurrentVehiculesWithKeys.size

            if (nbrOfKeyCollected == 0) {
                binding.ivDashboardNoKeys.isVisible = true
                binding.tvDashboardKeysInPossession.text =
                    getString(R.string.dashboard_no_keys_in_possession)
                binding.ivDashboardKeys.isVisible = false
            } else {
                binding.ivDashboardNoKeys.isVisible = false
                binding.ivDashboardKeys.isVisible = true
                binding.tvDashboardKeysInPossession.text =
                    getString(R.string.dashboard_keys_in_possession)
            }

            myListAdapter.submitList(listOfCurrentVehiculesWithKeys)
        })

        binding.btnDashboardChangeSite.setOnClickListener {
            navController.navigate(navToSelection)
        }

    }
}
