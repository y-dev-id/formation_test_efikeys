package fr.devid.app.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.SnapHelper
import dagger.hilt.android.AndroidEntryPoint
import fr.devid.app.R
import fr.devid.app.api.BoxStatusEnum
import fr.devid.app.databinding.FragmentDetailsKeyBinding


@AndroidEntryPoint
class DetailsKeyFragment : Fragment() {

    private var _binding: FragmentDetailsKeyBinding? = null
    private val binding get() = _binding!!

    lateinit var myAdapter: CarPicturesAdapter

    private val myViewModel: DetailsKeyViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailsKeyBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navController = findNavController()

        myAdapter = CarPicturesAdapter()
        binding.rvDetailsPictures.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = myAdapter
            val snapHelper: SnapHelper = LinearSnapHelper()
            snapHelper.attachToRecyclerView(this)
        }

        myViewModel.displayCar()

        myViewModel.carToDisplay.observe(viewLifecycleOwner, Observer {

            myAdapter.submitList(it.imageUrls)

            val color = when (it.boxStatus) {
                BoxStatusEnum.USED -> R.color.colorMainOrange
                else -> R.color.colorPrimaryDark
            }

            binding.clDetailsStatusKey.setBackgroundResource(color)
            binding.btnDetailsAskKey.setBackgroundColor(color)

            binding.tvDetailsPlaque.text = it.licensePlate
            binding.tvDetailsModele.text = it.type

            binding.tvDetailsFlotteName.text = it.contactName
            binding.tvDetailsFlotteTelephone.text = it.contactPhone
            binding.tvDetailsCarName.text = it.type
            binding.tvDetailsCarKm.text = it.kilometer.toString() + " km"
            binding.tvDetailsCarChassisNumber.text = it.chassis

            binding.btnDetailsAskKey.text =
                if (it.boxStatus == BoxStatusEnum.USED) "Demander la clé" else "Ouvrir le casier"

            if (it.boxStatus == BoxStatusEnum.USED) {
                binding.tvDetailsKeyPlaceUserName.text = "Clé en possesion de " + it.collectedBy
                binding.tvDetailsKeyPlaceDate.text = "depuis le " + it.clientRemarks
            } else {
                binding.tvDetailsKeyPlaceUserName.text = "Clé disponible"
                binding.ivDetailsKeyPlaceUser.visibility = View.INVISIBLE
                binding.tvDetailsKeyPlaceDate.visibility = View.INVISIBLE
            }
        })

        binding.ivDetailsClose.setOnClickListener {
            navController.popBackStack()
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
