package fr.devid.app.ui.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fr.devid.app.BuildConfig
import fr.devid.app.base.BaseFragment
import fr.devid.app.databinding.FragmentAccountBinding

@AndroidEntryPoint
class AccountFragment : BaseFragment() {

    private val accountViewModel: AccountViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentAccountBinding.inflate(inflater, container, false)

        val navController = findNavController()
        bindUi(binding, navController)
        //updateUi(binding, navController)

        return binding.root
    }

    private fun bindUi(binding: FragmentAccountBinding, navController: NavController) {
        if (BuildConfig.DEBUG) {
            binding.etFirstname.setText("Jonathan")
            binding.etLastname.setText("Doevile")
            binding.etEmail.setText("bob@test.com")
            binding.etPassword.setText("Aa1234567!")
        }
        /*binding.btUserAccountUpdate.setOnClickListener {

            accountViewModel.updateProfile(

                binding.etEmail.text?.toString() ?: "",
                binding.etPassword.text?.toString() ?: ""
            )
            navController.navigate(
                LoginFragmentDirections.actionLoginFragmentToRegisterFragment(
                    false
                )
            )
        }*/
    }

}
