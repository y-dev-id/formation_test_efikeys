package fr.devid.app.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fr.devid.app.BuildConfig
import fr.devid.app.R
import fr.devid.app.base.BaseFragment
import fr.devid.app.databinding.FragmentLoginBinding

import fr.devid.app.viewmodels.EventObserver

@AndroidEntryPoint
class LoginFragment : BaseFragment() {

    private val loginViewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentLoginBinding.inflate(inflater, container, false)

        val navController = findNavController()
        bindUi(binding, navController)
        subscribeUi(binding, navController)

        return binding.root
    }

    private fun bindUi(binding: FragmentLoginBinding, navController: NavController) {
        if (BuildConfig.DEBUG) {
            binding.etEmail.setText("bob@test.com")
            binding.etPassword.setText("Aa1234567!")
        }
        binding.btConnexion.setOnClickListener {

            loginViewModel.login(
                binding.etEmail.text?.toString() ?: "",
                binding.etPassword.text?.toString() ?: ""
            )
        }
        binding.mbGoToRegistration.setOnClickListener {
            navController.navigate(
                LoginFragmentDirections.actionLoginFragmentToRegisterFragment(
                    false
                )
            )
        }
        binding.mbForgotPassword.setOnClickListener {
        }
    }

    private fun subscribeUi(binding: FragmentLoginBinding, navController: NavController) {
        loginViewModel.authenticationState.observe(viewLifecycleOwner, {
            if (it == LoginViewModel.AuthenticationState.AUTHENTICATED) {
                navController.navigate(LoginFragmentDirections.actionLoginFragmentToMainFragment())
            }
        })
        loginViewModel.isLoading.observe(viewLifecycleOwner, { isLoading ->
            binding.pbLoading.isVisible = isLoading
            binding.btConnexion.isEnabled = !isLoading
        })
        loginViewModel.loginState.observe(viewLifecycleOwner, EventObserver {
            val errorResource = when (it) {
                LoginViewModel.LoginState.FILL_FIELDS -> R.string.fill_all_fields
                LoginViewModel.LoginState.NO_INTERNET -> R.string.check_internet
                LoginViewModel.LoginState.NOT_ACTIVATED -> R.string.not_activated
                LoginViewModel.LoginState.WRONG_CREDENTIALS -> R.string.wrong_credentials
            }
            Toast.makeText(context, errorResource, Toast.LENGTH_LONG).show()
        })
    }

}
