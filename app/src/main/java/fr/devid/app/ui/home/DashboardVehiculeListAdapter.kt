package fr.devid.app.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.devid.app.api.Vehicle
import fr.devid.app.databinding.DashboardItemRvVehiculesBinding

class DashboardVehiculeListAdapter :
    ListAdapter<Vehicle, DashboardVehiculeListAdapter.VehiculeHolder>(VehiculeDiffUtil()) {

    var onClickVehicle: ((Vehicle) -> Unit)? = null

    class VehiculeHolder(val binding: DashboardItemRvVehiculesBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehiculeHolder {
        val binding =
            DashboardItemRvVehiculesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )

        return VehiculeHolder(binding)
    }

    override fun onBindViewHolder(holder: VehiculeHolder, position: Int) {
        val vehicle: Vehicle = getItem(position)
        holder.binding.tvDashboardItemVehiculeCarModel.text = vehicle.type
        holder.binding.tvIdashboardItemVehiculeNumberplate.text = vehicle.licensePlate
        holder.binding.llDashboardItemVehicule.setOnClickListener {
            onClickVehicle?.invoke(vehicle)
        }

        if (vehicle.imageUrls.isNotEmpty()) {
            Glide.with(holder.binding.ivDashboardItemVehicule.context)
                .load(vehicle.imageUrls[0])
                .into(holder.binding.ivDashboardItemVehicule)
        }
    }
}

class VehiculeDiffUtil : DiffUtil.ItemCallback<Vehicle>() {
    override fun areItemsTheSame(oldItem: Vehicle, newItem: Vehicle): Boolean {
        return oldItem.licensePlate == newItem.licensePlate
    }

    override fun areContentsTheSame(oldItem: Vehicle, newItem: Vehicle): Boolean {
        return oldItem == newItem
    }
}
