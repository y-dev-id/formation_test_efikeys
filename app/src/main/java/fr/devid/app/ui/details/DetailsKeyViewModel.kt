package fr.devid.app.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.devid.app.api.AppService
import fr.devid.app.api.Vehicle
import javax.inject.Inject

@HiltViewModel
class DetailsKeyViewModel @Inject constructor(
    private val appService: AppService
) : ViewModel() {

    private val _carToDisplay = MutableLiveData<Vehicle>()
    val carToDisplay: LiveData<Vehicle> = _carToDisplay

    fun displayCar() {
        _carToDisplay.value = Vehicle.SAMPLE
    }
}
