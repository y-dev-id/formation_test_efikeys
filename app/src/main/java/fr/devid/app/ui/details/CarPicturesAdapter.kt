package fr.devid.app.ui.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.devid.app.databinding.ItemCarPictureBinding

class CarPicturesAdapter : ListAdapter<String, CarPicturesAdapter.CarPicturesHolder>(MyDiffUtil()) {

    lateinit var binding: ItemCarPictureBinding

    inner class CarPicturesHolder(val binding: ItemCarPictureBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarPicturesHolder {
        binding = ItemCarPictureBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return CarPicturesHolder(binding)
    }

    override fun onBindViewHolder(holder: CarPicturesHolder, position: Int) {
        val urlPicture: String = getItem(position)

        holder.binding.apply {
            Glide.with(binding.ivItemCarPicture.context)
                .load(urlPicture)
                .into(ivItemCarPicture)

            /*Picasso.get()
                .load(urlPicture)
                .into(ivItemCarPicture)*/
        }
    }
}

class MyDiffUtil : DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }
}

/*
class CarPicturesAdapter : RecyclerView.Adapter<CarPicturesHolder>() {

    private val pictures = mutableListOf<String>()
    lateinit var binding: ItemCarPictureBinding

    fun displayPictures(list : List<String>){
        pictures.clear()
        pictures.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarPicturesHolder {
        binding = ItemCarPictureBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CarPicturesHolder(binding)
    }

    override fun onBindViewHolder(holder: CarPicturesHolder, position: Int) {
        TODO("Not yet implemented")
    }

    override fun getItemCount(): Int {
        return pictures.size
    }
}

*/
