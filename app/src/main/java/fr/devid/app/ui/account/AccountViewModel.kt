package fr.devid.app.ui.account

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import fr.devid.app.api.AppService
import fr.devid.app.service.AuthenticationTokenInterceptor
import javax.inject.Inject

@HiltViewModel
class AccountViewModel @Inject constructor(
    private val authenticationTokenInterceptor: AuthenticationTokenInterceptor,
    private val appService: AppService
) : ViewModel()
