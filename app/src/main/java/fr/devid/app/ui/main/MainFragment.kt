package fr.devid.app.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import fr.devid.app.R
import fr.devid.app.base.BaseFragment
import fr.devid.app.databinding.FragmentMainBinding
import fr.devid.app.ui.login.LoginViewModel

@AndroidEntryPoint
class MainFragment : BaseFragment() {

    private val loginViewModel: LoginViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentMainBinding.inflate(inflater, container, false)
        val tabNavHostFragment =
            childFragmentManager.findFragmentById(R.id.tab_nav_host_fragment) as NavHostFragment
        val tabNavController = tabNavHostFragment.navController
        binding.bottomNavigation.setupWithNavController(tabNavController)

        val navController = findNavController()

        subscribeUi(navController)

        return binding.root
    }

    private fun subscribeUi(navController: NavController) {
        loginViewModel.authenticationState.observe(viewLifecycleOwner, {
            when (it) {
                LoginViewModel.AuthenticationState.UNAUTHENTICATED -> navController.navigate(
                    MainFragmentDirections.actionMainFragmentToLoginGraph()
                )
                else -> {
                } // Nothing to do
            }
        })
    }
}
