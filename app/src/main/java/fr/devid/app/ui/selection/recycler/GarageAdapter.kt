package fr.devid.app.ui.selection.recycler

import android.annotation.SuppressLint
import android.nfc.Tag
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.devid.app.R
import fr.devid.app.api.Garage
import fr.devid.app.api.GarageStatusEnum
import fr.devid.app.databinding.ItemRvGarageBinding
import timber.log.Timber

class GarageAdapter : ListAdapter<Garage, GarageAdapter.GarageHolder>(MyDiffUtil()) {
    inner class GarageHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    var selectedPosition = 0

    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int): GarageHolder {
        return GarageHolder(
            ItemRvGarageBinding
                .inflate(LayoutInflater
                    .from(parent.context), parent, false).root
        )
    }

    @SuppressLint("ResourceAsColor", "NotifyDataSetChanged")
    override fun onBindViewHolder(holder : GarageHolder, position: Int){
        var current = 0
        val garage = getItem(position)

        ItemRvGarageBinding.bind(holder.itemView).apply {
            tvTitleGarageRv.text = garage.name
            val status = when(garage.status){
                GarageStatusEnum.CONSTRUCTOR -> "Garage constructeur"
                else -> "Garage Indépendant"
            }

            val bgImageIntoStatus = when(garage.status){
                GarageStatusEnum.CONSTRUCTOR -> ivImgGarageRv.setBackgroundResource(R.drawable.bordersecond)
                else -> ivImgGarageRv.setBackgroundResource(R.drawable.border)
            }

            tvTextGarageRv.text = status

            if(garage.imageUrl.isEmpty()){
                ivImgGarageRv.setImageResource(R.drawable.car_icon)
            } else {
                Glide.with(holder.itemView)
                    .load(bgImageIntoStatus)
                    .into(ivImgGarageRv)
            }


            rbRv.isChecked = position == selectedPosition

            rbRv.setOnClickListener {
                notifyItemChanged(selectedPosition)
                selectedPosition = position
                current = selectedPosition
                notifyItemChanged(current)
            }
        }
    }
}

class MyDiffUtil : DiffUtil.ItemCallback<Garage>(){
    override fun areItemsTheSame(oldItem: Garage, newItem: Garage): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Garage, newItem: Garage): Boolean {
        return oldItem == newItem
    }

}
